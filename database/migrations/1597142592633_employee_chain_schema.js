'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmployeeChainSchema extends Schema {
  up () {
    this.create('employee_chains', (table) => {
      table.increments()
      table.integer('user_id',10).notNullable()
      table.integer('boss_id',10).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('employee_chains')
  }
}

module.exports = EmployeeChainSchema
