'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuestionSchema extends Schema {
  up () {
    this.create('questions', (table) => {
      table.increments()
      table.integer('project_id',10).notNullable()
      table.text('content').notNullable()
      table.integer('user_id',10).notNullable()
      table.boolean('isSolved').defaultTo(0)
      table.timestamps()
    })
  }

  down () {
    this.drop('questions')
  }
}

module.exports = QuestionSchema
