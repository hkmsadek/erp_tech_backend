'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserBalanceSchema extends Schema {
  up () {
    this.create('user_balances', (table) => {
      table.increments()
      table.integer('project_id',10).notNullable()
      table.integer('user_id',10).notNullable()
      table.string('type',191).notNullable()
      table.decimal('amount',8,2).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('user_balances')
  }
}

module.exports = UserBalanceSchema
