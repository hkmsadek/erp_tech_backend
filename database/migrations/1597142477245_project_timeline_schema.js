'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectTimelineSchema extends Schema {
  up () {
    this.create('project_timelines', (table) => {
      table.increments()
      table.integer('project_id',10).notNullable()
      table.integer('task_id',10).notNullable()
      table.text('content').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('project_timelines')
  }
}

module.exports = ProjectTimelineSchema
