'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Request extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }
    project() {
        return this.belongsTo('App/Models/Project')
    }
    request_type() {
        return this.belongsTo('App/Models/RequestType')
    }
}

module.exports = Request
