'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Question extends Model {
    user() {
        return this.belongsTo('App/Models/User')
    }
    project() {
        return this.belongsTo('App/Models/Project')
    }
    answer() {
        return this.hasMany('App/Models/Answer')
    }
}

module.exports = Question
