'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VideoRequest extends Model {
    project () {
        return this.belongsTo('App/Models/Project', 'project_id')
    }
    sender () {
        return this.belongsTo('App/Models/User', 'user_id')
    }
}

module.exports = VideoRequest
