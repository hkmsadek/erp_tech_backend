'use strict'
const User = use('App/Models/User')
class UserController {
   async user({request, response, params, auth}){
       try {
          const user = await auth.loginViaId(34)

       } catch (error) {
          return error
       }

   }

  async getUser({request, response, params, auth}){
    try {
        let user = await auth.getUser()
          return user
      } catch (error) {
          return
      }

  }
  async logout({request, response,session, params, auth}){
    session.clear()
    return await auth.logout()
}


  async login({ request, response, auth }){

        const { email, password } = request.all()
        let user = await User.query().where('email',email).first()
        if(user){
          if (user.user_type_id == 4) {
            return auth.attempt(email, password)
          } 
          else{
            return response.status(401).json(
              [{"message": "You're not an authenticated technician!"}]
            );
          }
        }
        else{
          return response.status(401).json(
            [{"message": "Invalid email or password. Please try again!"}]
          );
        }
    }
}

module.exports = UserController
