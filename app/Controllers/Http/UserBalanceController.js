'use strict'

const UserBalance = use('App/Models/UserBalance')

class UserBalanceController {
  // async allagentbalance ({ request, response, view,auth  }) {
  //     // let user
  //     // try {
  //     //   user = await auth.getUser()
  //     //   // let usertype = await UserType.query().where('id', user.user_type_id).first()
  //     //   // user.userType = usertype
  //     //   // if (usertype.name != 'Admin') {
  //     //   //   await auth.logout()
  //     //   //   return response.status(401).json({
  //     //   //     'message': 'Your are not authenticate user!!'
  //     //   //   })
  //     //   // }
  //     // } catch (error) {
  //     //   await auth.logout()
  //     //   return response.status(403).json({
  //     //     message: "You are not authenticate user"
  //     //   })
  //     // }

  //     let user = {}
  //     try {
  //         user = await auth.getUser()
  //     } catch (error) {
  //         return response.status(403).json({
  //             message: 'You are not authorized!'
  //         })
  //     }
  //   let data = request.all()
  //   let page = request.input('page') ? request.input('page') : 1
  //   let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
  //   let str = request.input('str') ? request.input('str') : ''
  //   let order = request.input('order') ? request.input('order') : 'desc'
  //   let colName = request.input('colName') ? request.input('colName') : 'id'

  //   let  q  =  UserBalance.query().with('user.usertype').where('user_id', user.id)
  //   if(str){

  //     q.whereHas('user', (builder) => {
  //       builder.where('firstname', 'LIKE', '%' + str + '%')
  //       .orWhere('lastname', 'LIKE', '%' + str + '%')
  //     })
  // }
    
  //   let alldata = await q.orderBy(colName, order).paginate(page, pageSize)
  //   return alldata


  // }

  async getAgentBalance ({ request, response, view ,auth }) {
    let user = await auth.getUser()
    const alldata = await  UserBalance.query().where('user_id', user.id).getSumDistinct('amount')
    return alldata
  }

  async allagentbalance ({ request, response, view,auth  }) {
    let user = await auth.getUser()

    let data = request.all()
    let page = request.input('page') ? request.input('page') : 1
    let pageSize = request.input('pageSize') ? request.input('pageSize') : 10
    let str = request.input('str') ? request.input('str') : ''
    let order = request.input('order') ? request.input('order') : 'desc'
    let colName = request.input('colName') ? request.input('colName') : 'id'

    let  q  =  UserBalance.query().with('project').where('user_id', user.id)
    if(str){

        q.whereHas('project', (builder) => {
        builder.where('name', 'LIKE', '%' + str + '%')
        })
    }
  
    let alldata = await q.orderBy(colName, order).paginate(page, pageSize)
    return alldata


  }

}

module.exports = UserBalanceController
