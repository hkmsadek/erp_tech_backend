'use strict'

const RequestType = use('App/Models/RequestType')
const Request= use('App/Models/Request')
const Database = use('Database')

class RequestController {
  
  async all_request_type_tech({request,response}){
    return await RequestType.all();
  }

  async timeExtend_req_tech({request,response}){
    let reqData = request.all()
    let res= await Request.create(reqData)
    if(res){
      return Request.query()
                    .with('user')
                    .with('project')
                    .with('request_type')
                    .orderBy('id','desc')
                    .first()
    }
    return;
  }

  async all_req_tech({request,response,auth}){
      let user = {}
      try {
          user = await auth.getUser()
      } catch (error) {
          return response.status(403).json({
              message: 'You are not authorized!'
          })
      }
      let page = request.input('page');
      let pageSize  = request.input('pageSize');
      let req_type = request.input('req_type');
      let searchKey = request.input('searchFilter');
      let query = Request.query()
  
      if(req_type=='Sent'){
        query.where('user_id', user.id)
      }
      else{
        query.where('receiver_id', user.id)
      }

      let res 
      if(searchKey){
        res = await query
        .with('user')
        .with('project')
        .with('request_type')
        .whereIn('id', (builder) => {
          builder.select('id')
              .from('requests')
              .orWhere(Database.raw(`description LIKE "%${searchKey}%"`))
              .orWhere(Database.raw(`status LIKE "%${searchKey}%"`))
          })
        .where('isDeleted',0)
        .orderBy('id','desc')
        .paginate(page,pageSize)
      }
      else{
        res = await query
        .with('user')
        .with('project')
        .with('request_type')
        .where('isDeleted',0)
        .orderBy('id','desc')
        .paginate(page,pageSize)
      }
      return res;
  }

  async request_delete_tech({request,response,params}){
    let data = request.all()
      return await Request.query()
                  .where('id',data.id)
                  .update({
                    isDeleted:1
                  })

  }

}

module.exports = RequestController
