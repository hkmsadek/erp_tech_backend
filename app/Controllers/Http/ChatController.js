"use strict";
const Conversation = use("App/Models/Conversation");
const Chat = use("App/Models/Chat");
const User = use("App/Models/User");

// classes
const NotificationClass = use("App/Classes/NotificationClass");
const Notification = new NotificationClass();
// helpers
const Database = use("Database");

class ChatController {



  // ********************************************** prospective client *****************************************************************
  async getClientConversationList({ request, response, auth }) {
    let user = {};
    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }
    const lists = await Conversation.query()
      .where('receiver', user.id)
      .with('agent')
      .with('chat')
      .fetch()

    return {
      conversation_lists: lists,
      // msgNotiCount: msgNotiCount/
    }

  }

  async getClientChatWithLastPerson({ request, response, auth, params }) {
    let user = {};
    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    if (!params.id) {
      return response.status(403).json({
        message: "Invalid Request!",
      });
    }

    var [chats, update, firstId] = await Promise.all([
      Chat.query()
        .where("conversation_id", params.id)
        .orderBy("id", "desc")
        .limit(15)
        .fetch(),
      Chat.query().where("conversation_id", params.id).update({
        seen: 1,
      }),
      Chat.query()
        .where("conversation_id", params.id)
        .select("id")
        .first(),
    ]);

    return {
      chats: this.formateChat(chats),
      firstId: firstId ? firstId.id : 0,
    };
  }
  // ********************************************** prospective client *****************************************************************














  async getConversationList({ request, response, auth }) {
    let user = {};

    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    const id = user.id;;
    const lists = await  Conversation.query()
    .where('receiver', user.id)
    .with('agent')
    .with('chat')
    .fetch()

  return {
    conversation_lists: lists,
    // msgNotiCount: msgNotiCount/
  }
}

  async getChatWithLastPerson({ request, response, auth, params }) {
    let user = {};
    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    if (!params.id) {
      return response.status(403).json({
        message: "Invalid Request!",
      });
    }

    let check = await Conversation.query()
      .where("id", params.id)
      .where((builder) => {
        builder.where("receiver", user.id);
        builder.orWhere("sender", user.id);
      })
      .getCount();

    if (check != 1) {
      return response.status(403).json({
        message: "Invalid request!",
      });
    }

    var [chats, update, firstId] = await Promise.all([
      Chat.query()
        .where("conversation_id", params.id)
        .where("deleted", "!=", user.id)
        .orderBy("id", "desc")
        .limit(15)
        .fetch(),
      Chat.query().where("conversation_id", params.id).update({
        seen: 1,
      }),
      Chat.query()
        .where("conversation_id", params.id)
        .where("deleted", "!=", user.id)
        .select("id")
        .first(),
    ]);

    return {
      chats: this.formateChat(chats),
      firstId: firstId ? firstId.id : 0,
    };
  }

  async getChatHistory({ request, response, auth, params }) {
    let user = {};
    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    if (!params.id) {
      return response.status(403).json({
        message: "Invalid request!",
      });
    }

    const id = user.id;
    // get the chat details where not equal to deleted and update seen
    const chat = await Chat.query()
      .where("conversation_id", params.id)
      .where("deleted", "!=", id)
      .limit(15)
      .orderBy("id", "desc")
      .fetch();

    if (!chat) {
      return response.status(200).json({
        chats: [],
      });
    }

    let c = chat.toJSON();

    c = c.reverse();
    if (c.length == 0) {
      return response.status(200).json({
        chats: [],
      });
    }
    // CHECK IF LAST MESSAGE IS SEEN OR NOT. IF seen===loggedin user id, mean it is not seen yet.
    if (
      c[c.length - 1].message_receiver == user.id &&
      c[c.length - 1].seen == 0
    ) {
      // this mean my id is stored in seen column so its not seen yet
      Chat.query()
        .where("id", c[c.length - 1].id)
        .update({
          seen: 1,
        });
      // NOW SEND SEEN NOTIFICATION TO OTHER USER...
      // noti.sendSeenNoti(params.id, c[c.length - 1].msg_sender, 'seen')
    }
    return response.status(200).json({
      chats: this.formateChat(chat),
    });
  }

  async getChatHistoryByUserId({ request, response, auth, params }) {
    let user = {};
    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    if (!params.id) {
      return response.status(403).json({
        message: "Invalid request!",
      });
    }

    let conversation_id = await Conversation.query()
      .where((builder) => {
        builder.where("sender", user.id);
        builder.where("receiver", params.id);
      })
      .orWhere((builder) => {
        builder.where("receiver", user.id);
        builder.where("sender", params.id);
      })
      .select("id")
      .first();
    console.log("ww", conversation_id);
    if (!conversation_id) {
      return response.status(200).json({
        chats: [],
      });
    }

    conversation_id = conversation_id.toJSON();
    const id = user.id;
    // get the chat details where not equal to deleted and update seen
    const chat = await Chat.query()
      .where("conversation_id", conversation_id.id)
      .where("deleted", "!=", id)
      .limit(15)
      .orderBy("id", "desc")
      .fetch();

    if (!chat) {
      return response.status(200).json({
        chats: [],
      });
    }

    let c = chat.toJSON();

    c = c.reverse();
    if (c.length == 0) {
      return response.status(200).json({
        chats: [],
      });
    }
    // CHECK IF LAST MESSAGE IS SEEN OR NOT. IF seen===loggedin user id, mean it is not seen yet.
    if (
      c[c.length - 1].message_receiver == user.id &&
      c[c.length - 1].seen == 0
    ) {
      // this mean my id is stored in seen column so its not seen yet
      Chat.query()
        .where("id", c[c.length - 1].id)
        .update({
          seen: 1,
        });
      // NOW SEND SEEN NOTIFICATION TO OTHER USER...
      // noti.sendSeenNoti(params.id, c[c.length - 1].msg_sender, 'seen')
    }
    return response.status(200).json({
      chats: this.formateChat(chat),
    });
  }

  async getMoreMessages({ request, response, auth, params }) {
    let user = {};

    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    const isOwner = await Conversation.query()
      .where("id", params.conId)
      .where((q) => {
        q.where("sender", user.id);
        q.orWhere("receiver", user.id);
      })
      .getCount();

    if (isOwner === 0) {
      return response
        .status(403)
        .json({ message: "Unauthorized request. Request has been blocked." });
    }

    let firstId = await Chat.query()
      .where("conversation_id", params.conId)
      .where("deleted", "!=", user.id)
      .select("id")
      .first();

    if (!firstId) {
      return {};
    }

    firstId = firstId.toJSON();
    if (firstId.id >= params.id) {
      return response.status(200).json({
        chats: [],
        firstId: 0,
      });
    }
    const chat = await Chat.query()
      .where("conversation_id", params.conId)
      .where("id", "<", params.id)
      .where("deleted", "!=", user.id)
      .orderBy("id", "desc")
      .limit(15)
      .fetch();

    return response.status(200).json({
      chats: this.formateChat(chat),
      firstId: firstId ? firstId.id : 0,
    });
  }

  async insertChat({ request, response, auth }) {
    let user = {};

    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    if (!request.body.receiver || !request.body.message) {
      return response.status(403).json({
        message: "Invalid Request!",
      });
    }

    const data = request.body;

    let receiver = await User.query().where("id", data.receiver).first();
    receiver = receiver.toJSON();

    // CHECK IF THERE ANY PREVIOUS CONVERSATIONS OR NOT
    const conversation = await Conversation.query()
      .whereRaw(
        `( sender = ${user.id} and receiver = ? ) or ( receiver = ${user.id} and sender = ? )`,
        [data.receiver, data.receiver]
      )
      .first();
    if (conversation) {
      // A PREVIOUS CONVERSATION IS FOUND
      const chat = await Chat.create({
        message_sender: user.id,
        message_receiver: data.receiver,
        attachment_id: null,
        conversation_id: conversation.id,
        message: data.message,
        seen: 0,
        deleted: 0,
      });

      if (receiver.deviceAppToken) {
        let obj = {
          user: user,
          conversation: conversation,
        };
        this.sendPushNotification(obj, receiver.deviceAppToken, data.message);
      }
      // LET THE RECEIVER KNOW
      Notification.sendChatWSNotificaiton(
        conversation.id,
        data.message,
        data.receiver,
        chat.id,
        user,
        data.lastMessages
      );
      return chat;
    } else {
      // create a new conversation...
      var con = await Conversation.create({
        sender: user.id,
        receiver: data.receiver,
      });

      if (con) {
        // now insert the message..
        const chat = await Chat.create({
          message_sender: user.id,
          message_receiver: data.receiver,
          conversation_id: con.id,
          message: data.message,
          seen: 0,
          deleted: 0,
        });
        if (receiver.deviceAppToken) {
          let obj = {
            user: user,
            conversation: con,
          };
          console.log("ob", obj);
          this.sendPushNotification(obj, receiver.deviceAppToken, data.message);
        }
        // LET THE RECEIVER KNOW
        Notification.sendChatWSNotificaiton(
          con.id,
          data.message,
          data.receiver,
          chat.id,
          user,
          data.lastMessages
        );
        return chat;
      }
    }
  }

  async getLastChatMessageToWS({ request, response, auth }) {
    let user = {};

    try {
      user = await auth.getUser();
    } catch (error) {
      return response.status(401).json({
        message: "You are not authorized!",
      });
    }

    if (!request.body.receiver || !request.body.chat_id) {
      return response.status(403).json({
        message: "Invalid Request!",
      });
    }
    // get the chat details where not equal to deleted and update seen
    let chat = await Chat.query()
      .where("id", request.body.chat_id)
      .orderBy("id", "desc")
      .first();
    return chat;
  }

  // static functions
  formateChat(chat) {
    chat = chat.toJSON();
    chat.reverse();
    return chat;
  }

  formateChatLists(chatLists) {
    return chatLists;
  }

  msgNotiCount(uid) {
    return Chat.query()
      .where("message_receiver", uid)
      .where("seen", 0)
      .getCount();
  }
}

module.exports = ChatController;
