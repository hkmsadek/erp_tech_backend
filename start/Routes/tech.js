const Route = use('Route')

Route.post('/tech/login', 'UserController.login')


//Project
Route.get('/tech/all_project_tech', 'ProjectController.all_project_tech')
Route.get('/tech/all_project_doc_tech', 'ProjectController.all_project_doc_tech')
Route.post('/tech/projectStatus', 'ProjectController.projectStatus')
Route.get('/tech/pending_project_count_tech', 'ProjectController.pending_project_count_tech')
Route.get('/tech/get_all_project_tech', 'ProjectController.get_all_project_tech')
Route.get('/tech/all_project_timeline_tech', 'ProjectController.all_project_timeline_tech')
Route.get('/tech/running_project_count_tech', 'ProjectController.running_project_count_tech')
Route.get('/tech/get_single_project', 'ProjectController.get_single_project')

Route.post('/tech/update_project_tech', 'ProjectController.update_project_tech')
Route.post('/tech/project_delete_tech', 'ProjectController.project_delete_tech')
Route.get('/tech/get_dashboad_project', 'ProjectController.get_dashboad_project')

//Task
Route.get('/tech/all_task_tech', 'TaskController.all_task_tech')
Route.post('/tech/taskStatus', 'TaskController.taskStatus')
Route.get('/tech/pending_task_count_tech','TaskController.pending_task_count_tech')

Route.post('/tech/task_delete_tech','TaskController.task_delete_tech')

//Question
Route.get('/tech/all_project_ques_tech', 'QuestionController.all_project_ques_tech')
Route.get('/tech/single_question_tech', 'QuestionController.single_question_tech')
Route.post('/tech/edit_question_tech', 'QuestionController.edit_question_tech')
Route.post('/tech/delete_ques_tech', 'QuestionController.delete_ques_tech')
Route.post('/tech/questionAdd', 'QuestionController.questionAdd')

//Answer
Route.get('/tech/answerShow', 'QuestionController.answerShow')
Route.post('/tech/answerAdd', 'QuestionController.answerAdd')
Route.post('/tech/edit_ans_tech', 'QuestionController.edit_ans_tech')
Route.post('/tech/delete_answer_tech', 'QuestionController.delete_answer_tech')

//Reply
Route.post('/tech/replyAdd', 'QuestionController.replyAdd') 
Route.get('/tech/allReplyShow', 'QuestionController.allReplyShow') 
Route.post('/tech/edit_reply_tech', 'QuestionController.edit_reply_tech') 
Route.post('/tech/delete_reply_tech', 'QuestionController.delete_reply_tech')

//Request
Route.get('/tech/all_request_type_tech', 'RequestController.all_request_type_tech')
Route.post('/tech/timeExtend_req_tech', 'RequestController.timeExtend_req_tech')
Route.get('/tech/all_req_tech', 'RequestController.all_req_tech')
Route.post('/tech/request_delete_tech', 'RequestController.request_delete_tech')


// Balance 
Route.get('/tech/getAgentBalance', 'UserBalanceController.getAgentBalance')
Route.get('/tech/allagentbalance', 'UserBalanceController.allagentbalance')


// Booking || Appointments
Route.get('/tech/appintments', 'VideoRequestController.appintments')
Route.post('/tech/appintments/editStatus', 'VideoRequestController.editAppointmentStatus')
Route.post('/tech/appintments/add', 'VideoRequestController.addAppointment')
//
Route.get('/tech/getClientList', 'VideoRequestController.getClientList')




//  *************************** Chat *********************************
// get chat list ( tech )
Route.get('/app/messenger/tech/conversation/get/list', 'ChatController.getConversationList')

// last person chat chat
Route.get('/app/messenger/tech/conversation/get/chatWithLastPerson/:id', 'ChatController.getChatWithLastPerson')

// get chat history by id
Route.get('/app/messenger/converstaion/:id', 'ChatController.getChatHistory')

// get chat history by username
Route.get('/app/messenger/chat/converstaion/:id', 'ChatController.getChatHistoryByUserId')

// get previous message of same chat
Route.get('/app/messenger/conversation/previous-chat/:conId/:id', 'ChatController.getMoreMessages')


// insert chat
Route.post('/app/messenger/chat/add', 'ChatController.insertChat')






// **************************************************************** ws ******************************************************************************
Route.post('/app/messenger/ws/get-last-chat', 'ChatController.getLastChatMessageToWS')
// **************************************************************** ws ******************************************************************************








